import { LandingComponent } from "./pages/landing/landing.component";
import { AddEventComponent } from './pages/addEvent/addEvent.component';

export const routes = [
    { 
        path: "", redirectTo: "/landing", pathMatch: "full"
    },
    {
        path: "landing", component: LandingComponent
    },
    {
        path: "iGet", component: LandingComponent
    },
    {
        path: "iPay", component: LandingComponent
    },
    {
        path: "addEvent", component: AddEventComponent
    }
];

export const navigatableComponents = [
    LandingComponent,
    AddEventComponent
];