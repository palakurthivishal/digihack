import { Injectable } from '@angular/core';
import * as appSettings from "application-settings";
import { getContactsWorker, getContactById } from 'nativescript-contacts-lite';

@Injectable()
export class ContactsPickerProvider {
  public cachedContacts:Array<any> = []; // array of objects containing shadow of entire phonebook

  constructor() {
      
  }

  /*
     creates a sorted cache of the phonebook through a web worker so UI won't get stuck and stores in app storage
  */
  public cacheContacts() {
    return new Promise((resolve, reject) => {
      console.log('Start caching contacts');

      var timer = new Date().getTime();

      /* get all contacts from phonebook */

      let desiredFields:Array<string> = ['display_name','phone']; // fields to fetch from contacts

      getContactsWorker(desiredFields).then((result) => {
        console.log(`getContactsWorker complete in ${(new Date().getTime() - timer)} ms. Found ${result.length} items.`);
        result= result.map((contact)=>{
          if(contact.phone && contact.phone.length){
            let mobileNumber= contact.phone.find((number)=>{
              return number.type === 'mobile';
            });
            if(mobileNumber)
              contact.phone = mobileNumber;
          }
          return contact;
        });
        this.cachedContacts = result;
        resolve();
      });
    });
  }
}