import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'contactsPicker'})
export class ContactsPicker implements PipeTransform {
  transform(contact): number {
    return contact.filteredOut;
  }
}