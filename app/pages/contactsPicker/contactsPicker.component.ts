import { Component, OnInit } from "@angular/core";
import { ContactsPickerProvider } from "./contactsPicker.provider";
import { TextField } from "ui/text-field";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";

@Component({
  selector: "contacts-picker-component",
  moduleId: module.id,
  templateUrl: './contactsPicker.component.html',
  styleUrls: ['./contactsPicker.component.css'],
  providers: [ContactsPickerProvider]
})
export class ContactsPickerComponent implements OnInit {
    contacts:any[];
    contactSearch:String='';
    filteredContacts:any[];
    constructor(
        private contactsPickerProvider:ContactsPickerProvider,
        private params:ModalDialogParams
    ){

    }
    ngOnInit(){
        this.contactsPickerProvider.cacheContacts().then(()=>{
            this.contacts= this.contactsPickerProvider.cachedContacts;
            this.contacts= this.contacts.map((c,index)=>{
                c.uId= index;
                return c;
            });
        });
        console.log(JSON.stringify(this.contacts));
    }
    
    onContactSearch(ev){
        let textField= <TextField>ev.object;
        this.contactSearch= textField.text;
        if(!this.contactSearch.length){
            this.contacts= this.contacts.map((c)=>{
                c.filteredOut= false;
                return c;
            });
            return;
        }
        this.contacts= this.contacts.map((contact)=>{
            if(contact.display_name.indexOf(this.contactSearch) > -1){
                contact.filteredOut= false;
            }else{
                contact.filteredOut= true;
            }

            return contact;
        });
    }
    toggleContactSelection(uId){
        this.contacts[uId].selected= !this.contacts[uId].selected;
    }
    submit(){
        let selectedContacts= this.contacts.filter((contact)=>{
            return contact.selected;
        });
        this.params.closeCallback(selectedContacts);
    }
}
