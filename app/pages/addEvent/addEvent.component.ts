import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { AddEventProvider } from './addEvent.provider';
import { ContactsPickerComponent } from '../contactsPicker/contactsPicker.component';
import { ModalDialogService } from "nativescript-angular/modal-dialog";

@Component({
  selector: "add-event-component",
  moduleId: module.id,
  templateUrl: './addEvent.component.html',
  styleUrls: ['./addEvent.component.css'],
  providers: [AddEventProvider]
})
export class AddEventComponent implements OnInit {
    private selectedContacts:any[];
  constructor(
    public addEventProvider:AddEventProvider, 
    private modalService:ModalDialogService,
    private vcRef: ViewContainerRef
    ){
  }
  ngOnInit() {
    
  }
  loadContacts(){
    // this.showContacts= true;
    this.modalService.showModal(ContactsPickerComponent,{
      viewContainerRef: this.vcRef,
      fullscreen: true
    }).then((selectedContacts)=>{
      console.log(JSON.stringify(this.selectedContacts));
      this.selectedContacts= selectedContacts;
    },()=>{

    });

  }
}
