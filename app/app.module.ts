import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { TNSFontIconModule } from 'nativescript-ng2-fonticon';
import { TNSCheckBoxModule } from 'nativescript-checkbox/angular';
import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { registerElement } from "nativescript-angular/element-registry";


import { AppComponent } from "./app.component";
import { ContactsPickerComponent } from "./pages/contactsPicker/contactsPicker.component";

import { routes, navigatableComponents } from "./app.routing";


@NgModule({
  declarations: [
    AppComponent,
    ContactsPickerComponent,
    ...navigatableComponents
  ],
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(routes),
    TNSFontIconModule.forRoot({
      'mdi': 'material-design-icons.css'
    }),
    TNSCheckBoxModule
  ],
  providers:[
    ModalDialogService
  ],
  entryComponents: [
    ContactsPickerComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
